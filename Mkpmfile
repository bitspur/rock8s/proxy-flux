include $(MKPM)/mkpm
include $(MKPM)/gnu

export ENABLED_MODULES := ndk lua subs-filter

DOCKER_COMPOSE ?= docker-compose
MINIFY ?= minify 
PYTHON ?= python3
TMPL ?= sh ./scripts/tmpl.sh

PORT ?= 8000
.PHONY: dev
dev:
	@$(CD) public
	@$(PYTHON) -mhttp.server $(PORT)

.PHONY: build
build: build/inject.min.html
build/inject.min.html.tmpl: public/index.html
	@$(MKDIR) -p build
	@$(CAT) public/index.html | \
		$(SED) -n '/<!-- FLOATING APP DIRECTORY START -->/,/<!-- FLOATING APP DIRECTORY END -->/p' | \
		$(MINIFY) --mime text/html | \
		$(SED) "s|\[FLOATING_APP_DIRECTORY_COLORS,\?\]|<% \$${FLOATING_APP_DIRECTORY_COLORS} %>|g" | \
		$(SED) "s|\[FLOATING_APP_DIRECTORY_APPS,\?\]|<% \$${FLOATING_APP_DIRECTORY_APPS} %>|g" > \
		$@
build/inject.min.html: build/inject.min.html.tmpl config.json
	@$(TMPL) $^ > $@

.PHONY: submodules
submodules:
	@$(GIT) submodule update --init --recursive

.PHONY: ingress-nginx/%
ingress-nginx/%: submodules
	@$(CD) $(@D)
	@$(MAKE) -s debian-image PREFIX=$@-with-subs-filter TARGET=download

.PHONY: docker/%
docker/%:
	@$(MAKE) -sC docker $(subst docker/,,$@) ARGS=$(ARGS)
