local _M = {}
local buffer_len = 7
local inject_content = ""
local inject_file_path = "/etc/nginx/lua/plugins/floating_app_directory/inject.min.html"
local io = io
local ngx = ngx
local string = string

function is_embedded_in_iframe()
    local referer = ngx.var.http_referer
    local host = ngx.var.host
    local uri = ngx.var.uri
    if not referer then return false end
    if string.find(referer, host .. uri) then
        return true
    end
    return false
end

function _M.init_worker()
    local file = io.open(inject_file_path, "r")
    if file then
        inject_content = file:read("*all")
        file:close()
    else
        ngx.log(ngx.ERR, "Failed to load " .. inject_file_path)
    end
end

function _M.header_filter()
    if not (ngx.var.floatingAppDirectory == "on") then
        return
    end
    local content_type = ngx.header["Content-Type"]
    if not (content_type and string.find(content_type, "html")) then
        return
    end
    if is_embedded_in_iframe() then
        return
    end
    ngx.header["Content-Length"] = nil
end

function _M.body_filter()
    if not (ngx.var.floatingAppDirectory == "on") then
        return
    end
    local content_type = ngx.header["Content-Type"]
    if not (content_type and string.find(content_type, "html")) then
        return
    end
    if is_embedded_in_iframe() then
        return
    end
    local ctx = ngx.ctx
    local body_buffer = ctx.body_buffer or ""
    local chunk = body_buffer .. ngx.arg[1]
    local modified_chunk, replacements = string.gsub(chunk, "</[Bb][Oo][Dd][Yy]>", inject_content:gsub("%%", "%%%%") .. "\n</body>")
    if ngx.arg[2] or replacements > 0 then
        ngx.arg[1] = modified_chunk
        ctx.body_buffer = nil
    else
        ctx.body_buffer = string.sub(modified_chunk, -buffer_len)
        ngx.arg[1] = string.sub(modified_chunk, 1, -buffer_len-1)
    end
end

return _M
