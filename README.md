# floating app directory

> an app directory that floats above all your web apps

★ ★ ★ Please star this project if you found it useful. ★ ★ ★

![](assets/demo.gif)

## Usage

### Inject into Kubernetes Ingress using nginx-ingress

1. Create the floating app directory config

   ```yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: floating-app-directory
     namespace: ingress-nginx
   data:
     config.json: |
       {
         "colors": ["#20B2AA", "#ADD8E6", "#98FB98", "#FFD700", "#DDA0DD", "#FF6347"],
         "apps": [
            {
              "title": "Example",
              "url": "https://example.com",
              "description": "An example app"
            }
         ]
       }
   ```

2. Add the following config to the ingress-nginx helm chart

   ```yaml
   controller:
     image:
       registry: registry.gitlab.com/bitspur/rock8s
       image: floating-app-directory
       tag: v1.8.0
       digest: ""
       digestChroot: ""
     config:
       plugins: floating_app_directory
     extraVolumes:
       - name: floating-app-directory
         configMap:
           name: floating-app-directory
     extraVolumeMounts:
       - name: floating-app-directory
         mountPath: /etc/nginx/lua/plugins/floating_app_directory/config.json
         subPath: config.json
         readOnly: true
   ```

3. Add the following annotation to the ingresses you want the floating app directory injected into

   ```yaml
   kind: Ingress
   metadata:
     annotations:
       nginx.ingress.kubernetes.io/configuration-snippet: |
         set $floatingAppDirectory "on";
   ```

### Inject into nginx openresty with lua

1. Update and add apps to the `config.json`.

2. Build the injection script with the following command.

   ```sh
   ./mkpm build
   ```

3. Move or copy the injection script from _build/inject.min.html_ to _/tmp/inject.min.html_.

4. Place this block inside the http context of your nginx.conf or the specific configuration file for openresty.

   ```lua
   local inject_content = ""
   init_by_lua_block {
       inject_content = nil
       local file = io.open("/tmp/inject.min.html", "r")
       if file then
           inject_content = file:read("*all")
           file:close()
       else
           ngx.log(ngx.ERR, "Failed to load /tmp/inject.min.html")
       end
   }
   ```

5. Place these blocks inside the specific location context where you want to manipulate the http response headers and body.

   ```lua
   local buffer_len = 7
   header_filter_by_lua_block {
       ngx.header["Content-Length"] = nil
   }
   body_filter_by_lua_block {
       local ctx = ngx.ctx
       local body_buffer = ctx.body_buffer or ""
       local chunk = body_buffer .. ngx.arg[1]
       local modified_chunk, replacements = string.gsub(chunk, "</[Bb][Oo][Dd][Yy]>", inject_content:gsub("%%", "%%%%") .. "\n</body>")
       if ngx.arg[2] or replacements > 0 then
           ngx.arg[1] = modified_chunk
           ctx.body_buffer = nil
       else
           ctx.body_buffer = string.sub(modified_chunk, -buffer_len)
           ngx.arg[1] = string.sub(modified_chunk, 1, -buffer_len-1)
       end
   }
   ```

6. Validate and reload nginx

   ```sh
   nginx -t
   nginx -s reload
   ```
