#!/bin/sh

tmpl /etc/nginx/lua/plugins/floating_app_directory/inject.min.html.tmpl \
    /etc/nginx/lua/plugins/floating_app_directory/config.json > \
    /etc/nginx/lua/plugins/floating_app_directory/inject.min.html

exec /usr/bin/_dumb-init "$@"
