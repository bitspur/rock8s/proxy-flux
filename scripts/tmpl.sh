#!/bin/sh

export FLOATING_APP_DIRECTORY_COLORS="$(cat "$2" | jq '.colors')"
export FLOATING_APP_DIRECTORY_APPS="$(cat "$2" | jq '.apps')"
EOF=EOF
exec cat <<EOF | sh | minify --mime text/html
cat <<EOF
$(cat $1 | \
    sed 's|\\|\\\\|g' | \
    sed 's|`|\\`|g' | \
    sed 's|\$|\\\$|g' | \
    sed "s|${OPEN:-<%}|\`echo |g" | \
    sed "s|${CLOSE:-%>}| 2>/dev/null \`|g")
$EOF
EOF
